﻿using Domain.AggregatesModel;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class AppDbContext : DbContext, IUnitOfWork
    {
        public DbSet<Anag_Account> Anag_Accounts { get; set; }
        public DbSet<Anag_Division> Anag_Divisions { get; set; }
        public DbSet<Anag_Master> Anag_Masters { get; set; }
        public DbSet<Anag_Medical> Anag_Medicals { get; set; }
        public DbSet<Anag_Role> Anag_Roles { get; set; }
        public DbSet<Anag_Score> Anag_Scores { get; set; }
        public DbSet<Anag_Weight> Anag_Weights { get; set; }

        public DbSet<Event_Athlete> Event_Athletes { get; set; }
        public DbSet<Event_Master> Event_Masters { get; set; }

        // Enumerations
        public DbSet<EN_Divisions> EN_Divisions { get; set; }
        public DbSet<EN_Levels> EN_Levels { get; set; }
        public DbSet<EN_Medicals> EN_Medicals { get; set; }
        public DbSet<EN_PaymentCausals> EN_PaymentCausals { get; set; }
        public DbSet<EN_PaymentTerms> EN_PaymentTerms { get; set; }
        public DbSet<EN_PaymentTypes> EN_PaymentTypes { get; set; }
        public DbSet<EN_ResultForTypes> EN_ResultForTypes { get; set; }
        public DbSet<EN_ResultTypes> EN_ResultTypes { get; set; }
        public DbSet<EN_Roles> EN_Roles { get; set; }
        public DbSet<EN_Sports> EN_Sports { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            //base.OnModelCreating(mb);

            mb.Entity<Anag_Master>(e =>
            {
                e.HasMany(p => p.Roles).WithOne(p => p.Master).HasForeignKey(p => p.AnagR_AnagID);
                e.HasMany(p => p.Medicals).WithOne(p => p.Master).HasForeignKey(p => p.AnagM_AnagID);
                e.HasMany(p => p.Weights).WithOne(p => p.Master).HasForeignKey(p => p.AnagW_AnagID);
                e.HasMany(p => p.Divisions).WithOne(p => p.Master).HasForeignKey(p => p.AnagD_AnagID);
                e.HasMany(p => p.Scores).WithOne(p => p.Master).HasForeignKey(p => p.AnagS_AnagID);
                e.HasOne(p => p.Account).WithOne(p => p.Master).HasForeignKey<Anag_Account>(p => p.AnagAcc_AnagID);
            });

            mb.Entity<Anag_Score>(e =>
            {
                e.HasMany(p => p.Events).WithOne(p => p.Score);
            });

            mb.Entity<Event_Master>(e =>
            {
                e.HasMany(p => p.Athletes).WithOne(p => p.Event).HasForeignKey(p => p.EvnA_EventID);
            });

            // Enumerations Seed Data
            mb.Entity<EN_Divisions>().HasData(
                new EN_Divisions(10, "Paglia", 52.2),
                new EN_Divisions(20, "Mosca", 56.7),
                new EN_Divisions(30, "Gallo", 61.2),
                new EN_Divisions(40, "Piuma", 65.8),
                new EN_Divisions(50, "Leggeri", 70.3),
                new EN_Divisions(60, "Welter", 77.1),
                new EN_Divisions(70, "Medi", 83.9),
                new EN_Divisions(80, "Massimi leggeri", 93.0),
                new EN_Divisions(90, "Massimi", 120.2));

            mb.Entity<EN_Levels>().HasData(
                new EN_Levels(10, "Dilettante"),
                new EN_Levels(20, "Semipro"),
                new EN_Levels(30, "Pro"));

            mb.Entity<EN_Medicals>().HasData(
                new EN_Medicals(10, "SPT", "Sportivo"),
                new EN_Medicals(15, "CMP", "Agonistico"));

            mb.Entity<EN_PaymentCausals>().HasData(
                new EN_PaymentCausals(10, "MBR", "Tesseramento"),
                new EN_PaymentCausals(20, "MSF", "Quota associativa"),
                new EN_PaymentCausals(30, "MSE", "Quota iscrizione evento"),
                new EN_PaymentCausals(40, "CNT", "Contributo volontario"));

            mb.Entity<EN_PaymentTerms>().HasData(
                new EN_PaymentTerms(10, "MTH", "Mensile"),
                new EN_PaymentTerms(15, "QTR", "Trimestrale"),
                new EN_PaymentTerms(20, "HYE", "Semestrale"),
                new EN_PaymentTerms(25, "YER", "Annuale"));

            mb.Entity<EN_PaymentTypes>().HasData(
                new EN_PaymentTypes(10, "CSH", "Contanti"),
                new EN_PaymentTypes(15, "CRD", "Carta"));

            mb.Entity<EN_ResultForTypes>().HasData(
                new EN_ResultForTypes(10, "Decisione unanime"),
                new EN_ResultForTypes(20, "Split decision"),
                new EN_ResultForTypes(30, "TKO"),
                new EN_ResultForTypes(40, "KO"),
                new EN_ResultForTypes(50, "Sottomissione"));

            mb.Entity<EN_ResultTypes>().HasData(
                new EN_ResultTypes(10, "Sconfitta"),
                new EN_ResultTypes(20, "Pareggio"),
                new EN_ResultTypes(30, "Vittoria"));

            mb.Entity<EN_Roles>().HasData(
                new EN_Roles(1, "GOD", "God Admin"),
                new EN_Roles(10, "PRS", "Presidente"),
                new EN_Roles(15, "VPR", "Vice presidente"),
                new EN_Roles(20, "SCR", "Segretario"),
                new EN_Roles(25, "TRS", "Tesoriere"),
                new EN_Roles(30, "ADV", "Consigliere"),
                new EN_Roles(35, "AAD", "Atleta Consigliere"),
                new EN_Roles(40, "ATH", "Atleta"));

            mb.Entity<EN_Sports>().HasData(
                new EN_Sports(10, "MMA"),
                new EN_Sports(20, "Pancrazio"),
                new EN_Sports(30, "Pankraton"),
                new EN_Sports(40, "Difesa Personale"),
                new EN_Sports(50, "Functional Training"));

            // Seed Data
            mb.Entity<Anag_Master>().HasData(
                new Anag_Master
                {
                    Anag_ID = 1,
                    Anag_TaxCode = "SPARTANADMIN",
                    Anag_FirstName = "Administrator",
                    Anag_LastName = "Spartan Club",
                    Anag_Alias = "God",
                    Anag_BirthDay = new DateTime(2008, 4, 9),
                    Anag_BirthCity = "Arezzo",
                    Anag_BirthCountrySpec = "AR",
                    Anag_BirthCountry = "ITA",
                    Anag_Address = "Loc. Il Matto 36",
                    Anag_City = "Arezzo",
                    Anag_CountrySpec = "AR",
                    AnagA_Country = "ITA",
                    Anag_Email = "info@spartanclubarezzo.it",
                    Anag_Mobile = "123123123",
                    Anag_Active = true
                });
            mb.Entity<Anag_Role>().HasData(
                new Anag_Role
                {
                    AnagR_ID = 1,
                    AnagR_AnagID = 1,
                    AnagR_RoleID = 1,
                    AnagR_From = new DateTime(2008, 4, 9)
                });
            mb.Entity<Anag_Account>().HasData( // psw SpartanGod2019!
                new Anag_Account
                {
                    AnagAcc_ID = 1,
                    AnagAcc_Active = true,
                    AnagAcc_AnagID = 1,
                    AnagAcc_Username = "Eracle",
                    AnagAcc_PasswordHash = "0b95b65062211e080f430c3d263f9fd1"
                });
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken)) { return (await base.SaveChangesAsync() > 0); }
    }
}
