﻿using Domain.AggregatesModel;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class AnagRepository : _BaseRepository<AppDbContext, Anag_Master, int>, IAnagRepository
    {
        public AnagRepository(AppDbContext context) : base(context) { }

        public async Task<Anag_Master> UpdateLevelAsync(int anagID, int level, int sport, DateTime? from = null, int? divisionID = null)
        {
            if (!_context.Set<EN_Levels>().Any(l => l.ID == level)) throw new Exception($"The level {level} does not exist.");
            if (!_context.Set<EN_Sports>().Any(l => l.ID == sport)) throw new Exception($"The sport {sport} does not exist.");

            var anag = await GetAsync(anagID);
            if (anag == null) throw new Exception("User not found");

            if (anag.Divisions.Any(d => !d.AnagW_To.HasValue && d.AnagD_LevelID == level && d.AnagD_SportID == sport)) return anag;

            var last = anag.Divisions.Where(d => !d.AnagW_To.HasValue && d.AnagD_SportID == sport).FirstOrDefault();
            if (last != null)
            {
                last.AnagW_To = from.HasValue ? from.Value.AddDays(-1) : DateTime.Today;
                _context.Entry(last).State = EntityState.Modified;
                _context.Entry(new Anag_Division
                {
                    AnagD_AnagID = last.AnagD_AnagID,
                    AnagD_DivisionID = last.AnagD_DivisionID,
                    AnagD_SportID = last.AnagD_SportID,
                    AnagD_LevelID = level,
                    AnagW_From = last.AnagW_To.Value.AddDays(1)
                }).State = EntityState.Added;
                await _context.SaveEntitiesAsync();
            }
            else
            {
                _context.Entry(new Anag_Division
                {
                    AnagD_AnagID = anag.Anag_ID,
                    AnagD_DivisionID = divisionID ?? _context.EN_Divisions.Min(o => o.ID),
                    AnagD_SportID = sport,
                    AnagD_LevelID = level,
                    AnagW_From = DateTime.Today
                }).State = EntityState.Added;
                await _context.SaveEntitiesAsync();
            }
            
            return await Task.FromResult(anag);
        }

        public async Task<Anag_Master> UpdateWeightAsync(int anagID, double weight, DateTime? from = null)
        {
            if (weight <= 0) throw new Exception($"{nameof(weight)} must me greater then zero");
            var anag = await GetAsync(anagID);
            if (anag == null) throw new Exception("User not found");
            var last = anag.Weights.Where(w => !w.AnagW_To.HasValue).FirstOrDefault();
            if(last != null)
            {
                last.AnagW_To = from.HasValue ? from.Value.AddDays(-1) : DateTime.Today;
                _context.Entry(last).State = EntityState.Modified;
                _context.Entry(new Anag_Weight
                {
                    AnagW_AnagID = last.AnagW_AnagID,
                    AnagW_Weight = weight,
                    AnagW_From = last.AnagW_To.Value.AddDays(1)
                }).State = EntityState.Added;
                await _context.SaveEntitiesAsync();
            }
            else
            {
                _context.Entry(new Anag_Weight
                {
                    AnagW_AnagID = anagID,
                    AnagW_Weight = weight,
                    AnagW_From = DateTime.Today
                }).State = EntityState.Added;
                await _context.SaveEntitiesAsync();
            }
            return await Task.FromResult(anag);
        }
    }
}
