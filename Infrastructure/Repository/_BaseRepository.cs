﻿using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using NSpecifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class _BaseRepository<TDbContext, TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
        where TEntity : class, IAggregateRoot
        where TDbContext : DbContext, IUnitOfWork
    {
        protected readonly TDbContext _context;
        public IUnitOfWork UnitOfWork { get => _context; }

        public _BaseRepository(TDbContext context) { _context = context ?? throw new ArgumentNullException(nameof(context)); }

        public TEntity Get(TPrimaryKey id) { return _context.Set<TEntity>().Find(id); }
        public async Task<TEntity> GetAsync(TPrimaryKey id) { return await _context.Set<TEntity>().FindAsync(id); }

        public IQueryable<TEntity> GetAll() { return _context.Set<TEntity>().AsQueryable(); }
        public async Task<IQueryable<TEntity>> GetAllAsync() { return await Task.FromResult(_context.Set<TEntity>().AsQueryable()); }

        public IQueryable<TEntity> GetBySpecifications(params ISpecification<TEntity>[] specs)
        {
            var entities = GetAll();
            if (specs != null)
                foreach (var spec in specs)
                    entities = entities.Include(spec.Expression);
            return entities;
        }

        public async Task<IQueryable<TEntity>> GetBySpecificationsAsync(params ISpecification<TEntity>[] specs)
        {
            var entities = await GetAllAsync();
            if (specs != null)
                foreach (var spec in specs)
                    entities = entities.Include(spec.Expression);
            return await Task.FromResult(entities);
        }

        public TEntity Add(TEntity entity)
        {
            var rtn = _context.Set<TEntity>().Add(entity).Entity;
            _context.SaveEntitiesAsync().Wait();
            return rtn;
        }
        public async Task<TEntity> AddAsync(TEntity entity)
        {
            var rtn = await Task.FromResult(_context.Set<TEntity>().Add(entity).Entity);
            await _context.SaveEntitiesAsync();
            return rtn;
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);
            _context.SaveEntitiesAsync().Wait();
        }
        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);
            await _context.SaveEntitiesAsync();
        }

        public TEntity Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveEntitiesAsync().Wait();
            return entity;
        }
        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveEntitiesAsync();
            return await Task.FromResult(entity);
        }

        public void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            _context.SaveEntitiesAsync().Wait();
        }
        public async Task RemoveAsync(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveEntitiesAsync();
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
            _context.SaveEntitiesAsync().Wait();
        }
        public async Task RemoveRangeAsync(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
            await _context.SaveEntitiesAsync();
        }

    }
}
