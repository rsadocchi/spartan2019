﻿using Domain.AggregatesModel;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class EventRepository : _BaseRepository<AppDbContext, Event_Master, int>, IEventRepository
    {
        public EventRepository(AppDbContext context) : base(context) { }

        public async Task<Event_Master> AddAthleteAsync(int evID, Event_Athlete athlete)
        {
            var ev = await GetAsync(evID);
            if (ev == null || athlete == null) throw new Exception("Event or athete to add unset");
            _context.Entry(athlete).State = EntityState.Added;
            await _context.SaveEntitiesAsync();
            return await Task.FromResult(ev);
        }

        public async Task UpdateScoreXAthleteAsync(int evID, int athleteID, int resultID, int resultForID)
        {
            var @event = await GetAsync(evID);
            var athlete = @event.Athletes.FirstOrDefault(a => a.EvnA_AthleteID == athleteID);
            athlete.Score.AnagS_ResultID = resultID;
            athlete.Score.AnagS_ResultForID = resultForID;
            await _context.SaveEntitiesAsync();
            await Task.CompletedTask;
        }
    }
}
