﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Spartan.API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Anag_Master",
                columns: table => new
                {
                    Anag_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Anag_TaxCode = table.Column<string>(maxLength: 16, nullable: false),
                    Anag_FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    Anag_LastName = table.Column<string>(maxLength: 50, nullable: false),
                    Anag_Alias = table.Column<string>(maxLength: 20, nullable: true),
                    Anag_BirthDay = table.Column<DateTime>(nullable: false),
                    Anag_BirthCity = table.Column<string>(maxLength: 50, nullable: false),
                    Anag_BirthCountrySpec = table.Column<string>(maxLength: 2, nullable: false),
                    Anag_BirthCountry = table.Column<string>(maxLength: 3, nullable: false),
                    Anag_Address = table.Column<string>(maxLength: 250, nullable: false),
                    Anag_City = table.Column<string>(maxLength: 50, nullable: false),
                    Anag_CountrySpec = table.Column<string>(maxLength: 2, nullable: false),
                    AnagA_Country = table.Column<string>(maxLength: 3, nullable: true),
                    Anag_Mobile = table.Column<string>(maxLength: 30, nullable: false),
                    Anag_Phone = table.Column<string>(maxLength: 30, nullable: true),
                    Anag_Email = table.Column<string>(maxLength: 100, nullable: true),
                    Anag_FirstStart = table.Column<DateTime>(nullable: true),
                    Anag_Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Master", x => x.Anag_ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_Diviosions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Kg = table.Column<double>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_Diviosions", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_Levels",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_Levels", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_Medicals",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_Medicals", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_PaymentCausals",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_PaymentCausals", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_PaymentTerms",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_PaymentTerms", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_PaymentTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_PaymentTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_ResultForTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_ResultForTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_ResultTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_ResultTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_Roles",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_Roles", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "EN_Sports",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EN_Sports", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Event_Master",
                columns: table => new
                {
                    Evn_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Evn_Date = table.Column<DateTime>(nullable: false),
                    Evn_Name = table.Column<string>(nullable: false),
                    Evn_Address = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event_Master", x => x.Evn_ID);
                });

            migrationBuilder.CreateTable(
                name: "Anag_Account",
                columns: table => new
                {
                    AnagAcc_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnagAcc_AnagID = table.Column<int>(nullable: false),
                    AnagAcc_Username = table.Column<string>(nullable: false),
                    AnagAcc_PasswordHash = table.Column<string>(nullable: false),
                    AnagAcc_Avatar = table.Column<string>(nullable: true),
                    AnagAcc_Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Account", x => x.AnagAcc_ID);
                    table.ForeignKey(
                        name: "FK_Anag_Account_Anag_Master_AnagAcc_AnagID",
                        column: x => x.AnagAcc_AnagID,
                        principalTable: "Anag_Master",
                        principalColumn: "Anag_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Anag_Division",
                columns: table => new
                {
                    AnagD_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnagD_AnagID = table.Column<int>(nullable: false),
                    AnagD_SportID = table.Column<int>(nullable: false),
                    AnagD_DivisionID = table.Column<int>(nullable: false),
                    AnagD_LevelID = table.Column<int>(nullable: false),
                    AnagW_From = table.Column<DateTime>(nullable: false),
                    AnagW_To = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Division", x => x.AnagD_ID);
                    table.ForeignKey(
                        name: "FK_Anag_Division_Anag_Master_AnagD_AnagID",
                        column: x => x.AnagD_AnagID,
                        principalTable: "Anag_Master",
                        principalColumn: "Anag_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Anag_Medical",
                columns: table => new
                {
                    AnagM_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnagM_AnagID = table.Column<int>(nullable: false),
                    AnagM_MedicalID = table.Column<int>(nullable: false),
                    AnagM_Release = table.Column<DateTime>(nullable: false),
                    AnagM_Expiring = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Medical", x => x.AnagM_ID);
                    table.ForeignKey(
                        name: "FK_Anag_Medical_Anag_Master_AnagM_AnagID",
                        column: x => x.AnagM_AnagID,
                        principalTable: "Anag_Master",
                        principalColumn: "Anag_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Anag_Role",
                columns: table => new
                {
                    AnagR_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnagR_AnagID = table.Column<int>(nullable: false),
                    AnagR_RoleID = table.Column<int>(nullable: false),
                    AnagR_From = table.Column<DateTime>(nullable: true),
                    AnagR_To = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Role", x => x.AnagR_ID);
                    table.ForeignKey(
                        name: "FK_Anag_Role_Anag_Master_AnagR_AnagID",
                        column: x => x.AnagR_AnagID,
                        principalTable: "Anag_Master",
                        principalColumn: "Anag_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Anag_Score",
                columns: table => new
                {
                    AnagS_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnagS_AnagID = table.Column<int>(nullable: false),
                    AnagS_SportID = table.Column<int>(nullable: false),
                    AnagS_EventID = table.Column<int>(nullable: false),
                    AnagS_ResultID = table.Column<int>(nullable: false),
                    AnagS_ResultForID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Score", x => x.AnagS_ID);
                    table.ForeignKey(
                        name: "FK_Anag_Score_Anag_Master_AnagS_AnagID",
                        column: x => x.AnagS_AnagID,
                        principalTable: "Anag_Master",
                        principalColumn: "Anag_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Anag_Weight",
                columns: table => new
                {
                    AnagW_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnagW_AnagID = table.Column<int>(nullable: false),
                    AnagW_Weight = table.Column<double>(nullable: false),
                    AnagW_From = table.Column<DateTime>(nullable: false),
                    AnagW_To = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anag_Weight", x => x.AnagW_ID);
                    table.ForeignKey(
                        name: "FK_Anag_Weight_Anag_Master_AnagW_AnagID",
                        column: x => x.AnagW_AnagID,
                        principalTable: "Anag_Master",
                        principalColumn: "Anag_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Event_Athlete",
                columns: table => new
                {
                    EvnA_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EvnA_EventID = table.Column<int>(nullable: false),
                    EvnA_AthleteID = table.Column<int>(nullable: false),
                    EnvA_LevelID = table.Column<int>(nullable: false),
                    EnvA_DivisionID = table.Column<int>(nullable: false),
                    ScoreAnagS_ID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event_Athlete", x => x.EvnA_ID);
                    table.ForeignKey(
                        name: "FK_Event_Athlete_Event_Master_EvnA_EventID",
                        column: x => x.EvnA_EventID,
                        principalTable: "Event_Master",
                        principalColumn: "Evn_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Event_Athlete_Anag_Score_ScoreAnagS_ID",
                        column: x => x.ScoreAnagS_ID,
                        principalTable: "Anag_Score",
                        principalColumn: "AnagS_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Anag_Master",
                columns: new[] { "Anag_ID", "AnagA_Country", "Anag_Active", "Anag_Address", "Anag_Alias", "Anag_BirthCity", "Anag_BirthCountry", "Anag_BirthCountrySpec", "Anag_BirthDay", "Anag_City", "Anag_CountrySpec", "Anag_Email", "Anag_FirstName", "Anag_FirstStart", "Anag_LastName", "Anag_Mobile", "Anag_Phone", "Anag_TaxCode" },
                values: new object[] { 1, "ITA", true, "Loc. Il Matto 36", "God", "Arezzo", "ITA", "AR", new DateTime(2008, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Arezzo", "AR", "info@spartanclubarezzo.it", "Administrator", null, "Spartan Club", "123123123", null, "SPARTANADMIN" });

            migrationBuilder.InsertData(
                table: "EN_Diviosions",
                columns: new[] { "ID", "Code", "Kg" },
                values: new object[,]
                {
                    { 80, "Massimi leggeri", 93.0 },
                    { 70, "Medi", 83.9 },
                    { 60, "Welter", 77.1 },
                    { 50, "Leggeri", 70.3 },
                    { 90, "Massimi", 120.2 },
                    { 30, "Gallo", 61.2 },
                    { 20, "Mosca", 56.7 },
                    { 10, "Paglia", 52.2 },
                    { 40, "Piuma", 65.8 }
                });

            migrationBuilder.InsertData(
                table: "EN_Levels",
                columns: new[] { "ID", "Code" },
                values: new object[,]
                {
                    { 10, "Dilettante" },
                    { 20, "Semipro" },
                    { 30, "Pro" }
                });

            migrationBuilder.InsertData(
                table: "EN_Medicals",
                columns: new[] { "ID", "Code", "Description" },
                values: new object[,]
                {
                    { 10, "SPT", "Sportivo" },
                    { 15, "CMP", "Agonistico" }
                });

            migrationBuilder.InsertData(
                table: "EN_PaymentCausals",
                columns: new[] { "ID", "Code", "Description" },
                values: new object[,]
                {
                    { 40, "CNT", "Contributo volontario" },
                    { 10, "MBR", "Tesseramento" },
                    { 20, "MSF", "Quota associativa" },
                    { 30, "MSE", "Quota iscrizione evento" }
                });

            migrationBuilder.InsertData(
                table: "EN_PaymentTerms",
                columns: new[] { "ID", "Code", "Description" },
                values: new object[,]
                {
                    { 20, "HYE", "Semestrale" },
                    { 25, "YER", "Annuale" },
                    { 10, "MTH", "Mensile" },
                    { 15, "QTR", "Trimestrale" }
                });

            migrationBuilder.InsertData(
                table: "EN_PaymentTypes",
                columns: new[] { "ID", "Code", "Description" },
                values: new object[,]
                {
                    { 10, "CSH", "Contanti" },
                    { 15, "CRD", "Carta" }
                });

            migrationBuilder.InsertData(
                table: "EN_ResultForTypes",
                columns: new[] { "ID", "Code" },
                values: new object[,]
                {
                    { 10, "Decisione unanime" },
                    { 20, "Split decision" },
                    { 30, "TKO" },
                    { 40, "KO" },
                    { 50, "Sottomissione" }
                });

            migrationBuilder.InsertData(
                table: "EN_ResultTypes",
                columns: new[] { "ID", "Code" },
                values: new object[,]
                {
                    { 10, "Sconfitta" },
                    { 20, "Pareggio" },
                    { 30, "Vittoria" }
                });

            migrationBuilder.InsertData(
                table: "EN_Roles",
                columns: new[] { "ID", "Code", "Description" },
                values: new object[,]
                {
                    { 40, "ATH", "Atleta" },
                    { 35, "AAD", "Atleta Consigliere" },
                    { 30, "ADV", "Consigliere" },
                    { 25, "TRS", "Tesoriere" },
                    { 1, "GOD", "God Admin" },
                    { 15, "VPR", "Vice presidente" },
                    { 10, "PRS", "Presidente" },
                    { 20, "SCR", "Segretario" }
                });

            migrationBuilder.InsertData(
                table: "EN_Sports",
                columns: new[] { "ID", "Code" },
                values: new object[,]
                {
                    { 40, "Difesa Personale" },
                    { 10, "MMA" },
                    { 20, "Pancrazio" },
                    { 30, "Pankraton" },
                    { 50, "Functional Training" }
                });

            migrationBuilder.InsertData(
                table: "Anag_Account",
                columns: new[] { "AnagAcc_ID", "AnagAcc_Active", "AnagAcc_AnagID", "AnagAcc_Avatar", "AnagAcc_PasswordHash", "AnagAcc_Username" },
                values: new object[] { 1, true, 1, null, "0b95b65062211e080f430c3d263f9fd1", "Eracle" });

            migrationBuilder.InsertData(
                table: "Anag_Role",
                columns: new[] { "AnagR_ID", "AnagR_AnagID", "AnagR_From", "AnagR_RoleID", "AnagR_To" },
                values: new object[] { 1, 1, new DateTime(2008, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null });

            migrationBuilder.CreateIndex(
                name: "IX_Anag_Account_AnagAcc_AnagID",
                table: "Anag_Account",
                column: "AnagAcc_AnagID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Anag_Division_AnagD_AnagID",
                table: "Anag_Division",
                column: "AnagD_AnagID");

            migrationBuilder.CreateIndex(
                name: "IX_Anag_Medical_AnagM_AnagID",
                table: "Anag_Medical",
                column: "AnagM_AnagID");

            migrationBuilder.CreateIndex(
                name: "IX_Anag_Role_AnagR_AnagID",
                table: "Anag_Role",
                column: "AnagR_AnagID");

            migrationBuilder.CreateIndex(
                name: "IX_Anag_Score_AnagS_AnagID",
                table: "Anag_Score",
                column: "AnagS_AnagID");

            migrationBuilder.CreateIndex(
                name: "IX_Anag_Weight_AnagW_AnagID",
                table: "Anag_Weight",
                column: "AnagW_AnagID");

            migrationBuilder.CreateIndex(
                name: "IX_Event_Athlete_EvnA_EventID",
                table: "Event_Athlete",
                column: "EvnA_EventID");

            migrationBuilder.CreateIndex(
                name: "IX_Event_Athlete_ScoreAnagS_ID",
                table: "Event_Athlete",
                column: "ScoreAnagS_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Anag_Account");

            migrationBuilder.DropTable(
                name: "Anag_Division");

            migrationBuilder.DropTable(
                name: "Anag_Medical");

            migrationBuilder.DropTable(
                name: "Anag_Role");

            migrationBuilder.DropTable(
                name: "Anag_Weight");

            migrationBuilder.DropTable(
                name: "EN_Diviosions");

            migrationBuilder.DropTable(
                name: "EN_Levels");

            migrationBuilder.DropTable(
                name: "EN_Medicals");

            migrationBuilder.DropTable(
                name: "EN_PaymentCausals");

            migrationBuilder.DropTable(
                name: "EN_PaymentTerms");

            migrationBuilder.DropTable(
                name: "EN_PaymentTypes");

            migrationBuilder.DropTable(
                name: "EN_ResultForTypes");

            migrationBuilder.DropTable(
                name: "EN_ResultTypes");

            migrationBuilder.DropTable(
                name: "EN_Roles");

            migrationBuilder.DropTable(
                name: "EN_Sports");

            migrationBuilder.DropTable(
                name: "Event_Athlete");

            migrationBuilder.DropTable(
                name: "Event_Master");

            migrationBuilder.DropTable(
                name: "Anag_Score");

            migrationBuilder.DropTable(
                name: "Anag_Master");
        }
    }
}
