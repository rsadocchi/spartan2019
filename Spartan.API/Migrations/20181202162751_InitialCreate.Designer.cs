﻿// <auto-generated />
using System;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Spartan.API.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20181202162751_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Account", b =>
                {
                    b.Property<int>("AnagAcc_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("AnagAcc_Active");

                    b.Property<int>("AnagAcc_AnagID");

                    b.Property<string>("AnagAcc_Avatar");

                    b.Property<string>("AnagAcc_PasswordHash")
                        .IsRequired();

                    b.Property<string>("AnagAcc_Username")
                        .IsRequired();

                    b.HasKey("AnagAcc_ID");

                    b.HasIndex("AnagAcc_AnagID")
                        .IsUnique();

                    b.ToTable("Anag_Account");

                    b.HasData(
                        new { AnagAcc_ID = 1, AnagAcc_Active = true, AnagAcc_AnagID = 1, AnagAcc_PasswordHash = "0b95b65062211e080f430c3d263f9fd1", AnagAcc_Username = "Eracle" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Division", b =>
                {
                    b.Property<int>("AnagD_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AnagD_AnagID");

                    b.Property<int>("AnagD_DivisionID");

                    b.Property<int>("AnagD_LevelID");

                    b.Property<int>("AnagD_SportID");

                    b.Property<DateTime>("AnagW_From");

                    b.Property<DateTime?>("AnagW_To");

                    b.HasKey("AnagD_ID");

                    b.HasIndex("AnagD_AnagID");

                    b.ToTable("Anag_Division");
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Master", b =>
                {
                    b.Property<int>("Anag_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AnagA_Country")
                        .HasMaxLength(3);

                    b.Property<bool>("Anag_Active");

                    b.Property<string>("Anag_Address")
                        .IsRequired()
                        .HasMaxLength(250);

                    b.Property<string>("Anag_Alias")
                        .HasMaxLength(20);

                    b.Property<string>("Anag_BirthCity")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Anag_BirthCountry")
                        .IsRequired()
                        .HasMaxLength(3);

                    b.Property<string>("Anag_BirthCountrySpec")
                        .IsRequired()
                        .HasMaxLength(2);

                    b.Property<DateTime>("Anag_BirthDay");

                    b.Property<string>("Anag_City")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Anag_CountrySpec")
                        .IsRequired()
                        .HasMaxLength(2);

                    b.Property<string>("Anag_Email")
                        .HasMaxLength(100);

                    b.Property<string>("Anag_FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime?>("Anag_FirstStart");

                    b.Property<string>("Anag_LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Anag_Mobile")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<string>("Anag_Phone")
                        .HasMaxLength(30);

                    b.Property<string>("Anag_TaxCode")
                        .IsRequired()
                        .HasMaxLength(16);

                    b.HasKey("Anag_ID");

                    b.ToTable("Anag_Master");

                    b.HasData(
                        new { Anag_ID = 1, AnagA_Country = "ITA", Anag_Active = true, Anag_Address = "Loc. Il Matto 36", Anag_Alias = "God", Anag_BirthCity = "Arezzo", Anag_BirthCountry = "ITA", Anag_BirthCountrySpec = "AR", Anag_BirthDay = new DateTime(2008, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), Anag_City = "Arezzo", Anag_CountrySpec = "AR", Anag_Email = "info@spartanclubarezzo.it", Anag_FirstName = "Administrator", Anag_LastName = "Spartan Club", Anag_Mobile = "123123123", Anag_TaxCode = "SPARTANADMIN" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Medical", b =>
                {
                    b.Property<int>("AnagM_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AnagM_AnagID");

                    b.Property<DateTime>("AnagM_Expiring");

                    b.Property<int>("AnagM_MedicalID");

                    b.Property<DateTime>("AnagM_Release");

                    b.HasKey("AnagM_ID");

                    b.HasIndex("AnagM_AnagID");

                    b.ToTable("Anag_Medical");
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Role", b =>
                {
                    b.Property<int>("AnagR_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AnagR_AnagID");

                    b.Property<DateTime?>("AnagR_From");

                    b.Property<int>("AnagR_RoleID");

                    b.Property<DateTime?>("AnagR_To");

                    b.HasKey("AnagR_ID");

                    b.HasIndex("AnagR_AnagID");

                    b.ToTable("Anag_Role");

                    b.HasData(
                        new { AnagR_ID = 1, AnagR_AnagID = 1, AnagR_From = new DateTime(2008, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), AnagR_RoleID = 1 }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Score", b =>
                {
                    b.Property<int>("AnagS_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AnagS_AnagID");

                    b.Property<int>("AnagS_EventID");

                    b.Property<int>("AnagS_ResultForID");

                    b.Property<int>("AnagS_ResultID");

                    b.Property<int>("AnagS_SportID");

                    b.HasKey("AnagS_ID");

                    b.HasIndex("AnagS_AnagID");

                    b.ToTable("Anag_Score");
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Weight", b =>
                {
                    b.Property<int>("AnagW_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AnagW_AnagID");

                    b.Property<DateTime>("AnagW_From");

                    b.Property<DateTime?>("AnagW_To");

                    b.Property<double>("AnagW_Weight");

                    b.HasKey("AnagW_ID");

                    b.HasIndex("AnagW_AnagID");

                    b.ToTable("Anag_Weight");
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_Divisions", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.Property<double>("Kg")
                        .HasMaxLength(30);

                    b.HasKey("ID");

                    b.ToTable("EN_Diviosions");

                    b.HasData(
                        new { ID = 10, Code = "Paglia", Kg = 52.2 },
                        new { ID = 20, Code = "Mosca", Kg = 56.7 },
                        new { ID = 30, Code = "Gallo", Kg = 61.2 },
                        new { ID = 40, Code = "Piuma", Kg = 65.8 },
                        new { ID = 50, Code = "Leggeri", Kg = 70.3 },
                        new { ID = 60, Code = "Welter", Kg = 77.1 },
                        new { ID = 70, Code = "Medi", Kg = 83.9 },
                        new { ID = 80, Code = "Massimi leggeri", Kg = 93.0 },
                        new { ID = 90, Code = "Massimi", Kg = 120.2 }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_Levels", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.HasKey("ID");

                    b.ToTable("EN_Levels");

                    b.HasData(
                        new { ID = 10, Code = "Dilettante" },
                        new { ID = 20, Code = "Semipro" },
                        new { ID = 30, Code = "Pro" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_Medicals", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.HasKey("ID");

                    b.ToTable("EN_Medicals");

                    b.HasData(
                        new { ID = 10, Code = "SPT", Description = "Sportivo" },
                        new { ID = 15, Code = "CMP", Description = "Agonistico" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_PaymentCausals", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.HasKey("ID");

                    b.ToTable("EN_PaymentCausals");

                    b.HasData(
                        new { ID = 10, Code = "MBR", Description = "Tesseramento" },
                        new { ID = 20, Code = "MSF", Description = "Quota associativa" },
                        new { ID = 30, Code = "MSE", Description = "Quota iscrizione evento" },
                        new { ID = 40, Code = "CNT", Description = "Contributo volontario" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_PaymentTerms", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.HasKey("ID");

                    b.ToTable("EN_PaymentTerms");

                    b.HasData(
                        new { ID = 10, Code = "MTH", Description = "Mensile" },
                        new { ID = 15, Code = "QTR", Description = "Trimestrale" },
                        new { ID = 20, Code = "HYE", Description = "Semestrale" },
                        new { ID = 25, Code = "YER", Description = "Annuale" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_PaymentTypes", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.Property<string>("Description")
                        .HasMaxLength(30);

                    b.HasKey("ID");

                    b.ToTable("EN_PaymentTypes");

                    b.HasData(
                        new { ID = 10, Code = "CSH", Description = "Contanti" },
                        new { ID = 15, Code = "CRD", Description = "Carta" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_ResultForTypes", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.HasKey("ID");

                    b.ToTable("EN_ResultForTypes");

                    b.HasData(
                        new { ID = 10, Code = "Decisione unanime" },
                        new { ID = 20, Code = "Split decision" },
                        new { ID = 30, Code = "TKO" },
                        new { ID = 40, Code = "KO" },
                        new { ID = 50, Code = "Sottomissione" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_ResultTypes", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.HasKey("ID");

                    b.ToTable("EN_ResultTypes");

                    b.HasData(
                        new { ID = 10, Code = "Sconfitta" },
                        new { ID = 20, Code = "Pareggio" },
                        new { ID = 30, Code = "Vittoria" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_Roles", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("ID");

                    b.ToTable("EN_Roles");

                    b.HasData(
                        new { ID = 1, Code = "GOD", Description = "God Admin" },
                        new { ID = 10, Code = "PRS", Description = "Presidente" },
                        new { ID = 15, Code = "VPR", Description = "Vice presidente" },
                        new { ID = 20, Code = "SCR", Description = "Segretario" },
                        new { ID = 25, Code = "TRS", Description = "Tesoriere" },
                        new { ID = 30, Code = "ADV", Description = "Consigliere" },
                        new { ID = 35, Code = "AAD", Description = "Atleta Consigliere" },
                        new { ID = 40, Code = "ATH", Description = "Atleta" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.EN_Sports", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("Code");

                    b.HasKey("ID");

                    b.ToTable("EN_Sports");

                    b.HasData(
                        new { ID = 10, Code = "MMA" },
                        new { ID = 20, Code = "Pancrazio" },
                        new { ID = 30, Code = "Pankraton" },
                        new { ID = 40, Code = "Difesa Personale" },
                        new { ID = 50, Code = "Functional Training" }
                    );
                });

            modelBuilder.Entity("Domain.AggregatesModel.Event_Athlete", b =>
                {
                    b.Property<int>("EvnA_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EnvA_DivisionID");

                    b.Property<int>("EnvA_LevelID");

                    b.Property<int>("EvnA_AthleteID");

                    b.Property<int>("EvnA_EventID");

                    b.Property<int?>("ScoreAnagS_ID");

                    b.HasKey("EvnA_ID");

                    b.HasIndex("EvnA_EventID");

                    b.HasIndex("ScoreAnagS_ID");

                    b.ToTable("Event_Athlete");
                });

            modelBuilder.Entity("Domain.AggregatesModel.Event_Master", b =>
                {
                    b.Property<int>("Evn_ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Evn_Address")
                        .IsRequired();

                    b.Property<DateTime>("Evn_Date");

                    b.Property<string>("Evn_Name")
                        .IsRequired();

                    b.HasKey("Evn_ID");

                    b.ToTable("Event_Master");
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Account", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Anag_Master", "Master")
                        .WithOne("Account")
                        .HasForeignKey("Domain.AggregatesModel.Anag_Account", "AnagAcc_AnagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Division", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Anag_Master", "Master")
                        .WithMany("Divisions")
                        .HasForeignKey("AnagD_AnagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Medical", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Anag_Master", "Master")
                        .WithMany("Medicals")
                        .HasForeignKey("AnagM_AnagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Role", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Anag_Master", "Master")
                        .WithMany("Roles")
                        .HasForeignKey("AnagR_AnagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Score", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Anag_Master", "Master")
                        .WithMany("Scores")
                        .HasForeignKey("AnagS_AnagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Domain.AggregatesModel.Anag_Weight", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Anag_Master", "Master")
                        .WithMany("Weights")
                        .HasForeignKey("AnagW_AnagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Domain.AggregatesModel.Event_Athlete", b =>
                {
                    b.HasOne("Domain.AggregatesModel.Event_Master", "Event")
                        .WithMany("Athletes")
                        .HasForeignKey("EvnA_EventID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Domain.AggregatesModel.Anag_Score", "Score")
                        .WithMany("Events")
                        .HasForeignKey("ScoreAnagS_ID");
                });
#pragma warning restore 612, 618
        }
    }
}
