﻿using AutoMapper;
using Domain.AggregatesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spartan.API.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Alias { get; set; }
        public string Avatar { get; set; }
        public bool PasswordExpired { get; set; }
        public string Role { get; set; }

        public class ProfileConfig : AutoMapper.Profile
        {
            public ProfileConfig(string profilename = nameof(ProfileConfig)) : base(profilename)
            {
                CreateMap<Anag_Account, UserViewModel>(); //mappare per bene

                CreateMap<UserViewModel, Anag_Account>();

            }
        }
    }
}
