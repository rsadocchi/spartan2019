﻿using AutoMapper;
using Domain.AggregatesModel;
using Infrastructure;
using Spartan.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Spartan.API.Controllers
{
    [Authorize]
    [EnableCors("AllowAll")]
    [Route("API/[Controller]")]
    public class AuthController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ISecurityRepository _securityRepo;
        public AuthController(
            IMapper mapper,
            ISecurityRepository securityRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _securityRepo = securityRepository ?? throw new ArgumentNullException(nameof(securityRepository));
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] CredentialsViewModel credentials)
        {
            
            return NotFound("Invalid username or password");
        }

        [HttpGet("Ping/{token:string}")]
        public async Task<IActionResult> Ping(string token)
        {
            
            return BadRequest();
        }

        [HttpGet("Logout")]
        public async Task<IActionResult> Logout()
        {
            return Ok();
        }
    }
}