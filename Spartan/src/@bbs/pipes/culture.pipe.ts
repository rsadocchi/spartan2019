import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'bbsDate' })
export class CultureDatePipe extends DatePipe implements PipeTransform {

  transform(source: Date, format: string, culture?: string) {

    if (culture == undefined) culture = 'en-GB';
    // override solo per it o it-It
    if (culture.startsWith('it')) {
      switch (format) {
        case 'shortDate':
          return super.transform(source, 'dd/MM/yyyy');
        case 'mediumDate':
          return super.transform(source, 'dd MMM yyyy');
        case 'longDate':
          return super.transform(source, 'EEE dd MMM yyyy');
        case 'fullDate':
          return super.transform(source, 'EEEE dd MMMM yyyy');
        case 'stepperDate':
          return super.transform(source, 'd MMM');
        case 'medium':
          return super.transform(source, 'EEEE dd MMM yyyy - HH:mm');
        case 'time':
          return super.transform(source, 'HH:mm');
        default:
          return super.transform(source, format);
      }
    }

    // per tutte le lingue diverse da it o it-It
    switch (format) {
      case 'stepperDate':
        return super.transform(source, 'MMM d');
      case 'time':
        return super.transform(source, 'HH:mm');
      default:
        return super.transform(source, format);
    }
  }
}
