import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CultureDatePipe } from './culture.pipe';

@NgModule({
  declarations: [CultureDatePipe],
  imports: [CommonModule],
  exports: [CultureDatePipe]
})
export class CustomPipeModule { }
