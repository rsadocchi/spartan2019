import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ILocale } from "../interfaces/locale.interface";

@Injectable()
export class BBSTranslateService {

  constructor(private _translate: TranslateService) { }

  public loadLocales(...langs: ILocale[]) {
    const locales = [...langs];
    let isoCode = 'en-GB';
    locales.forEach((locale) => {
      this._translate.setTranslation(locale.lang, locale.data, true);
      if (locale.lang.startsWith(navigator.language)) { isoCode = locale.lang; }
    });
    this.useCulture(isoCode);
  }

  public useCulture(isoCode: string) {
    this._translate.use(isoCode);
    //console.log(isoCode);
  }
}
