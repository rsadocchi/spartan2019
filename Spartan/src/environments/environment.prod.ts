
export const environment = {
  production: true,
  console: {
    error: false,
    info: true,
    log: false,
  },
  api: {
    url: 'https://www.spartanclubarezzo.it/API'
  }
};
