
export const environment = {
  production: false,
  console: {
    error: true,
    info: true,
    log: true,
  },
  api: {
    url:'https://localhost:44338/API'
  }
};

