import { NgModule, ModuleWithProviders, LOCALE_ID } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
    MatButtonModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatCardModule,
    MatSelectModule,
    MAT_DATE_LOCALE,
    MAT_DATE_FORMATS
} from '@angular/material';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BBSTranslateService } from '@bbs/services/bbs.translate.service';
import localeEN from '@angular/common/locales/en';
import localeIT from '@angular/common/locales/it';
import { registerLocaleData } from '@angular/common';
import { AppComponent } from './app.component';
import { NavigatorComponent } from './content/navigator/navigator.component';
import { HomeComponent } from './content/home/home.component';
import { AboutComponent } from './content/about/about.component';
import { MixedMartialArtsComponent } from './content/about/mixed-martial-arts/mixed-martial-arts.component';
import { PankrationComponent } from './content/about/pankration/pankration.component';
import { FunctionalTrainingComponent } from './content/about/functional-training/functional-training.component';
import { FindUsComponent } from './content/find-us/find-us.component';
import { GymArezzoComponent } from './content/find-us/gym-arezzo/gym-arezzo.component';
import { GymSociComponent } from './content/find-us/gym-soci/gym-soci.component';
import { ContactComponent } from './content/find-us/contact/contact.component';
registerLocaleData(localeEN);
registerLocaleData(localeIT);

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];
export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);

@NgModule({
    declarations: [
        AppComponent,
        NavigatorComponent,
        HomeComponent,
        AboutComponent, MixedMartialArtsComponent, PankrationComponent, FunctionalTrainingComponent,
        FindUsComponent, GymArezzoComponent, GymSociComponent, ContactComponent
    ],
    imports: [
        routingModule,
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        TranslateModule.forRoot(),
        MatButtonModule,
        MatButtonToggleModule,
        MatDialogModule,
        MatInputModule,
        MatCheckboxModule,
        MatIconModule,
        MatToolbarModule,
        MatSnackBarModule,
        MatCardModule,
        MatSelectModule,
    ],
    providers: [
        TranslateService,
        BBSTranslateService,
        { provide: LOCALE_ID, useValue: navigator.language },
        { provide: MAT_DATE_LOCALE, useValue: navigator.language },
        { provide: MAT_DATE_FORMATS, useValue: MAT_DATE_FORMATS }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
