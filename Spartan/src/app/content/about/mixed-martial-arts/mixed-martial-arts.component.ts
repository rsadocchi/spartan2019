import { Component } from '@angular/core';

export const WORD_MIXED = 'mixed';
export const WORD_MARTIAL = 'martial';
export const WORD_ARTS = 'arts';

@Component({
    selector: 'mixed-martial-arts',
    templateUrl: './mixed-martial-arts.component.html',
    styleUrls: ['./mixed-martial-arts.component.scss']
})
export class MixedMartialArtsComponent {

    public mixed: string = '';
    public martial: string = '';
    public arts: string = '';

    private timer: any;

    constructor() {
        setInterval(() => {
            if (this.mixed.length < WORD_MIXED.length) {
                let idx = this.mixed.length;
                this.mixed += WORD_MIXED.charAt(idx);
            }
            else if (this.martial.length < WORD_MARTIAL.length) {
                let idx = this.martial.length;
                this.martial += WORD_MARTIAL.charAt(idx);
            }
            else if (this.arts.length < WORD_ARTS.length) {
                let idx = this.arts.length;
                this.arts += WORD_ARTS.charAt(idx);
            }
            else {
                this.mixed = '';
                this.martial = '';
                this.arts = '';
            }
        }, 350);
    }

}
