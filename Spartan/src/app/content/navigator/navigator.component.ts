import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'bbs-navigator',
    templateUrl: './navigator.component.html',
    styleUrls: ['./navigator.component.scss']
})
export class NavigatorComponent {

    constructor(/*private _router: Router*/) {

    }

    public goTo(anchor: string) {
        var element = document.getElementById(anchor);
        element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }
}
