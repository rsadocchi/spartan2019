﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.AggregatesModel
{
    public interface IEventRepository : IRepository<Event_Master, int>
    {
        Task<Event_Master> AddAthleteAsync(int evID, Event_Athlete athlete);
        Task UpdateScoreXAthleteAsync(int evID, int athleteID, int resultID, int resultForID);
    }
}
