﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Event_Athlete")]
    public class Event_Athlete : Entity, IAggregateRoot
    {
        [Key] public int EvnA_ID { get; set; }
        [Required] public int EvnA_EventID { get; set; }
        [Required] public int EvnA_AthleteID { get; set; }
        [Required] public int EnvA_LevelID { get; set; }
        [Required] public int EnvA_DivisionID { get; set; }

        public virtual Event_Master Event { get; set; }
        public virtual Anag_Score Score { get; set; }
    }
}
