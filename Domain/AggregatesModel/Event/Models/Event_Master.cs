﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Event_Master")]
    public class Event_Master : Entity, IAggregateRoot
    {
        [Key] public int Evn_ID { get; set; }
        [Required] public DateTime Evn_Date { get; set; }
        [Required] public string Evn_Name { get; set; }
        [Required] public string Evn_Address { get; set; }

        public ICollection<Event_Athlete> Athletes { get; set; }

        public Event_Master()
        {
            Athletes = new HashSet<Event_Athlete>();
        }
    }
}
