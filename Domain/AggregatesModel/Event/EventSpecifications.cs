﻿using NSpecifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.AggregatesModel
{
    public static class EventSpecifications
    {
        public static ASpec<Event_Master> ByAthlete(int athleteID)
        {
            return new Spec<Event_Master>(o => o.Athletes.Any(a => a.EvnA_ID == athleteID));
        }

        public static ASpec<Event_Master> ByAthletes(int[] athleteIDs)
        {
            return new Spec<Event_Master>(o => o.Athletes.Any(a => athleteIDs.Contains(a.EvnA_ID)));
        }

        public static ASpec<Event_Master> SearchByString(string key)
        {
            return new Spec<Event_Master>(o => o.Evn_Name.ToLower().Contains(key.ToLower()) || o.Evn_Address.ToLower().Contains(key.ToLower()));
        }

        public static ASpec<Event_Master> InDateRange(DateTime from, DateTime? limit = null)
        {
            ASpec<Event_Master> query = new Spec<Event_Master>(o => o.Evn_Date >= from);
            if (limit.HasValue && limit.Value > from)
                query = query & new Spec<Event_Master>(o => o.Evn_Date <= limit.Value);
            return query;
        }

        public static ASpec<Event_Master> Completed(DateTime? start = null, DateTime? end = null)
        {
            ASpec<Event_Master> query = new Spec<Event_Master>(o => !o.Athletes.Any(a => a.Score == null));
            if (start.HasValue)
                query = query & new Spec<Event_Master>(o => o.Evn_Date <= start.Value);
            if (end.HasValue || (start.HasValue && end.HasValue && end.Value > start.Value))
                query = query & new Spec<Event_Master>(o => o.Evn_Date <= end.Value);
            return query;
        }
    }
}
