﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Security")]
    public class Security : Entity, IAggregateRoot
    {
        [Key] public int Security_ID { get; set; }
        [Required] public int Security_AccountID { get; set; }
        [Required] public string Security_Token { get; set; }
        [Required] public DateTime Security_Login { get; set; }
        [Required] public DateTime Security_Expire { get; set; }
    }
}
