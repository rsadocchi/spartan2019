﻿using Domain.SeedWork;
using System.Threading.Tasks;

namespace Domain.AggregatesModel
{
    public interface ISecurityRepository : IRepository<Security, int>
    {
        Task<Anag_Account> LoginAsync(string username, string password);
        Task<bool> PingAsync();
        Task<bool> ChangePasswordAsync(int accID, string password);
        Task<string> ChangeAvatarAsync(int accID, string b64);
        Task LogoutAsync(int accID);
    }
}
