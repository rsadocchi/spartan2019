﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_PaymentCausals")]
    public class EN_PaymentCausals : Enumeration<int>
    {
        [StringLength(30)] public string Description { get; set; }

        public EN_PaymentCausals() { }
        public EN_PaymentCausals(int id, string code, string description) : base(id, code)
        {
            Description = description;
        }
    }
}
