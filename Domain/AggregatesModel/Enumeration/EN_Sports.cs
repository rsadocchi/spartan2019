﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_Sports")]
    public class EN_Sports : Enumeration<int>
    {
        public EN_Sports() { }
        public EN_Sports(int id, string code) : base(id, code) { }
    }
}
