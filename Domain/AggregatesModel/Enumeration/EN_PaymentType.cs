﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_PaymentTypes")]
    public class EN_PaymentTypes : Enumeration<int>
    {
        [StringLength(30)] public string Description { get; set; }

        public EN_PaymentTypes() { }
        public EN_PaymentTypes(int id, string code, string description) : base(id, code)
        {
            Description = description;
        }
    }
}
