﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_PaymentTerms")]
    public class EN_PaymentTerms : Enumeration<int>
    {
        [StringLength(30)] public string Description { get; set; }

        public EN_PaymentTerms() { }
        public EN_PaymentTerms(int id, string code, string description) : base(id, code)
        {
            Description = description;
        }
    }
}
