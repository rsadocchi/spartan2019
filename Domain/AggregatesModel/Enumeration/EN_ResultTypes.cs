﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_ResultTypes")]
    public class EN_ResultTypes : Enumeration<int>
    {
        public EN_ResultTypes() { }
        public EN_ResultTypes(int id, string code) : base(id, code) { }
    }
}
