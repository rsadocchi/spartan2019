﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_ResultForTypes")]
    public class EN_ResultForTypes : Enumeration<int>
    {
        public EN_ResultForTypes() { }
        public EN_ResultForTypes(int id, string code) : base(id, code) { }

    }
}
