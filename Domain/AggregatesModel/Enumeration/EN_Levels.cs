﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_Levels")]
    public class EN_Levels : Enumeration<int>
    {
        public EN_Levels() { }
        public EN_Levels(int id, string code) : base(id, code) { }
    }
}
