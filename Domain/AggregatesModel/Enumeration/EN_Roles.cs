﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_Roles")]
    public class EN_Roles : Enumeration<int>
    {
        [StringLength(50)] public string Description { get; set; }
        public EN_Roles() { }
        public EN_Roles(int id, string code, string description) : base(id, code) { Description = description; }
        
    }
}
