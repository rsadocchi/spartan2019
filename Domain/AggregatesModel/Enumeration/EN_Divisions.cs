﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_Diviosions")]
    public class EN_Divisions : Enumeration<int>
    {
        public double Kg { get; set; }

        public EN_Divisions() { }
        public EN_Divisions(int id, string code, double kg) : base(id, code) { Kg = kg; }
    }
}
