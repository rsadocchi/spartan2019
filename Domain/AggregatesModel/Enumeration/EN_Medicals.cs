﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "EN_Medicals")]
    public class EN_Medicals : Enumeration<int>
    {
        [StringLength(30)] public string Description { get; set; }

        public EN_Medicals() { }
        public EN_Medicals(int id, string code, string description) : base(id, code) { Description = description; }
    }
}
