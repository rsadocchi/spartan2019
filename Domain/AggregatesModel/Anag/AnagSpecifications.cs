﻿using NSpecifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.AggregatesModel
{
    public static class AnagSpecifications
    {
        public static ASpec<Anag_Master> ByDivision(int division, bool active = true)
        {
            ASpec<Anag_Master> result = new Spec<Anag_Master>(o => o.Divisions.Where(d => d.AnagD_DivisionID == division).Count() > 0);
            if (active)
                result = result & new Spec<Anag_Master>(o => o.Divisions.Where(d => !d.AnagW_To.HasValue).Count() > 0);
            return result;
        }

        public static ASpec<Anag_Master> ByWeightRange(double min, double? max = null, bool active = true)
        {
            ASpec<Anag_Master> result = new Spec<Anag_Master>(o => o.Weights.Where(w => w.AnagW_Weight >= min).Count() > 0);
            if (max.HasValue)
                result = result & new Spec<Anag_Master>(o => o.Weights.Where(w => w.AnagW_Weight <= max.Value).Count() > 0);
            if (active)
                result = result & new Spec<Anag_Master>(o => o.Weights.Where(w => !w.AnagW_To.HasValue).Count() > 0);
            return result;
        }

        public static ASpec<Anag_Master> ByLevel(int level, bool active = true)
        {
            ASpec<Anag_Master> result = new Spec<Anag_Master>(o => o.Divisions.Where(d => d.AnagD_LevelID == level).Count() > 0);
            if (active)
                result = result & new Spec<Anag_Master>(o => o.Divisions.Where(d => !d.AnagW_To.HasValue).Count() > 0);
            return result;
        }

        public static ASpec<Anag_Master> ByRole(int role, bool active = true)
        {
            ASpec<Anag_Master> result = new Spec<Anag_Master>(o => o.Roles.Where(r => r.AnagR_RoleID == role).Count() > 0);
            if (active)
                result = result & new Spec<Anag_Master>(o => o.Roles.Where(r => !r.AnagR_To.HasValue).Count() > 0);
            return result;
        }
    }
}
