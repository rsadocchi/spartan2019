﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.AggregatesModel
{
    public interface IAnagRepository : IRepository<Anag_Master, int>
    {
        Task<Anag_Master> UpdateWeightAsync(int anagID, double weight, DateTime? from = null);
        Task<Anag_Master> UpdateLevelAsync(int anagID, int level, int sport, DateTime? from = null, int? divisionID = null);
    }
}
