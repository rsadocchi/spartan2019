﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Medical")]
    public class Anag_Medical : Entity, IAggregateRoot
    {
        [Key] public int AnagM_ID { get; set; }
        [Required] public int AnagM_AnagID { get; set; }
        [Required] public int AnagM_MedicalID { get; set; }
        [Required] public DateTime AnagM_Release { get; set; }
        [Required] public DateTime AnagM_Expiring { get; set; }

        public virtual Anag_Master Master { get; set; }
    }
}
