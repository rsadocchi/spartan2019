﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Account")]
    public class Anag_Account : Entity, IAggregateRoot
    {
        [Key] public int AnagAcc_ID { get; set; }
        [Required] public int AnagAcc_AnagID { get; set; }
        [Required] public string AnagAcc_Username { get; set; }
        [Required] public string AnagAcc_PasswordHash { get; set; }
        public string AnagAcc_Avatar { get; set; }
        [Required] public bool AnagAcc_Active { get; set; }

        public virtual Anag_Master Master { get; set; }
    }
}
