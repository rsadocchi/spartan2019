﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Role")]
    public class Anag_Role : Entity, IAggregateRoot
    {
        [Key] public int AnagR_ID { get; set; }
        [Required] public int AnagR_AnagID { get; set; }
        [Required] public int AnagR_RoleID { get; set; }
        public DateTime? AnagR_From { get; set; }
        public DateTime? AnagR_To { get; set; }

        public virtual Anag_Master Master { get; set; }
    }
}
