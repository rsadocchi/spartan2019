﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Division")]
    public class Anag_Division : Entity, IAggregateRoot
    {
        [Key] public int AnagD_ID { get; set; }
        [Required] public int AnagD_AnagID { get; set; }
        [Required] public int AnagD_SportID { get; set; }
        [Required] public int AnagD_DivisionID { get; set; }
        [Required] public int AnagD_LevelID { get; set; }
        [Required] public DateTime AnagW_From { get; set; }
        public DateTime? AnagW_To { get; set; }


        public virtual Anag_Master Master { get; set; }
    }
}
