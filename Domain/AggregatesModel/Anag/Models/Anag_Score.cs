﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Score")]
    public class Anag_Score : Entity, IAggregateRoot
    {
        [Key] public int AnagS_ID { get; set; }
        [Required] public int AnagS_AnagID { get; set; }
        [Required] public int AnagS_SportID { get; set; }
        [Required] public int AnagS_EventID { get; set; }
        [Required] public int AnagS_ResultID { get; set; }
        [Required] public int AnagS_ResultForID { get; set; }
        
        public virtual Anag_Master Master { get; set; }
        public virtual ICollection<Event_Athlete> Events { get; set; }

        public Anag_Score()
        {
            Events = new HashSet<Event_Athlete>();
        }
    }
}
