﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Weight")]
    public class Anag_Weight : Entity, IAggregateRoot
    {
        [Key] public int AnagW_ID { get; set; }
        [Required] public int AnagW_AnagID { get; set; }
        [Required] public double AnagW_Weight { get; set; }
        [Required] public DateTime AnagW_From { get; set; }
        public DateTime? AnagW_To { get; set; }


        public virtual Anag_Master Master { get; set; }
    }
}
