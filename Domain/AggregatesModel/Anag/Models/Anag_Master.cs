﻿using Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.AggregatesModel
{
    [Table(name: "Anag_Master")]
    public class Anag_Master : Entity, IAggregateRoot
    {
        [Key] public int Anag_ID { get; set; }
        [Required] [StringLength(16)] public string Anag_TaxCode { get; set; }
        [Required] [StringLength(50)] public string Anag_FirstName { get; set; }
        [Required] [StringLength(50)] public string Anag_LastName { get; set; }
        [StringLength(20)] public string Anag_Alias { get; set; }
        [Required] public DateTime Anag_BirthDay { get; set; }
        [Required] [StringLength(50)] public string Anag_BirthCity { get; set; }
        [Required] [StringLength(2)] public string Anag_BirthCountrySpec { get; set; }
        [Required] [StringLength(3)] public string Anag_BirthCountry { get; set; }
        [Required] [StringLength(250)] public string Anag_Address { get; set; }
        [Required] [StringLength(50)] public string Anag_City { get; set; }
        [Required] [StringLength(2)] public string Anag_CountrySpec { get; set; }
        [StringLength(3)] public string AnagA_Country { get; set; }
        [Required] [StringLength(30)] public string Anag_Mobile { get; set; }
        [StringLength(30)] public string Anag_Phone { get; set; }
        [StringLength(100)] public string Anag_Email { get; set; }
        public DateTime? Anag_FirstStart { get; set; }
        [Required] public bool Anag_Active { get; set; }

        public virtual ICollection<Anag_Role> Roles { get; set; }
        public virtual ICollection<Anag_Medical> Medicals { get; set; }
        public virtual ICollection<Anag_Weight> Weights { get; set; }
        public virtual ICollection<Anag_Division> Divisions { get; set; }
        public virtual ICollection<Anag_Score> Scores { get; set; }
        public virtual Anag_Account Account { get; set; }

        public Anag_Master()
        {
            Roles = new HashSet<Anag_Role>();
            Medicals = new HashSet<Anag_Medical>();
            Weights = new HashSet<Anag_Weight>();
            Divisions = new HashSet<Anag_Division>();
            Scores = new HashSet<Anag_Score>();
        }
    }
}
